// ==UserScript==
// @name            Diary -> Dybr
// @namespace       http://tampermonkey.net/
// @description     Context menu to execute UserScript
// @version         0.0
// @author          Artalus & Denolia
// @include         /https?:\/\/(?!secure).*\.?diary.ru(\/.*)?/
// @grant           GM_registerMenuCommand
// @noframes
// ==/UserScript==]


(function () {
  'use strict';
  let is_active = () => (localStorage.exodus_is_active === 'true')
  let is_completed = () => (localStorage.exodus_is_completed === 'true')
  let was_in_process = () => (localStorage.exodus_diaryname !== undefined)
  let current_diary = () => (localStorage.exodus_diaryname)

  let page = () => (localStorage.exodus_page)
  let last_processed_post = () => (localStorage.exodus_last_processed_post)
  let working_post = () => (localStorage.exodus_working_post)

  logg(window.location.href)

  // UI: display alert with all possible info about import
  function import_status() {
    let msg = ''
    const URL = window.location.href
    msg += `Текущая веб-страница: ${URL}\n`
    const m = URL.match(/^https?:\/\/(.+).diary.ru/)
    if (!m) {
      msg += 'Вы находитесь не на diary.ru'
      alert(msg)
      return
    }
    msg += 'Процесс импорта дневника:\n'
    const d = current_diary()
    if (d)
      msg += (`- импортируемый дневник: ${d}.diary.ru\n`)
    else
      msg += '- дневник не выбран\n'
    if (is_active()) {
      msg += '- сейчас активен - продолжится АВТОМАТИЧЕСКИ при заходе на страницу дневника!\n'
    } else {
      msg += '- сейчас неактивен - можно безопасно заходить на страницу дневника\n'
      if (was_in_process())
        msg += '- ранее был запущен\n'
      else
        msg += '- ранее не запускался\n'
    }
    if (is_completed()) {
      msg += '- успешно завершён\n'
    } else {
      const p = page()
      if (p)
        msg += `- обрабатываемая страница: ${p}\n`
      const l = last_processed_post()
      if (l)
        msg += `- последний импортированный пост: ${l}\n`
      const w = working_post()
      if (w)
        msg += `- пост к обработке: ${w}\n`
    }
    alert(msg)
  }

  // UI: abort import and erase all data about process
  function import_abort() {
    const yes = confirm(`Это полностью прервёт импорт дневника ${current_diary()} и сотрёт данные о процессе.\nУверены?`)
    if (!yes)
      return
    remove_local_data()
    logg('import aborted')
    alert('Процесс импорта полностью остановлен')
  }

  function remove_local_data() {
    localStorage.removeItem('exodus_is_active')
    localStorage.removeItem('exodus_is_completed')
    localStorage.removeItem('exodus_diaryname')
    localStorage.removeItem('exodus_page')
    localStorage.removeItem('exodus_last_processed_post')
    localStorage.removeItem('exodus_working_post')
    localStorage.removeItem('exodus_prev_page_first_post')
    localStorage.removeItem('exodus_comments_mode')
    localStorage.removeItem('exodus_last_processed_comment')
  }

  // UI: begin/continue import
  function import_start() {
    logg('import_start()')

    const previous = current_diary()
    if (is_active()) {
      alert(`Уже идёт импорт дневника ${previous}.\n`
        + "Вначале прервите его или поставьте на паузу")
      return
    }

    const URL = window.location.href
    const m = URL.match(/^https?:\/\/(.+).diary.ru/)
    if (!m) {
      alert(`Вы находитесь не на diary.ru, а на ${URL}`
        + '\nВначале зайдите на свой дневник, например foobar.diary.ru')
      return
    }
    const selected = m[1]

    if (is_completed()) {
      const yes = confirm(`Дневник ${selected} уже был скачан.\nНачать сначала?`)
      if (!yes)
        return
      remove_local_data()
    } else {
      const yes = confirm(`Будет импортирован дневник ${selected}.\nУверены?`)
      if (!yes)
        return
    }
    localStorage.exodus_is_active = true
    localStorage.exodus_diaryname = selected
    logg('starting import')
    alert(`Начинаем импорт дневника ${selected}.`)
    if (!page()) {
      logg(`no page; starting a new import. setting current page:\n${URL}`)
      localStorage.exodus_page = URL
    }
    parse_current_window()
  }

  // UI: pause import until next "start" action
  function import_stop() {
    logg('stopping import')
    localStorage.exodus_is_active = false
    alert(`Импорт дневника ${current_diary()} остановлен`)
  }

  // main worker function
  // async to be able to sleep() inside and catch exceptions from inner functions
  async function parse_current_window() {
    try {
      while (true) { // iterating over posts on the page
        logg(`page() ${page()}`)
        logg(`comment mode: ${localStorage.exodus_comments_mode}`)

        if (is_completed())
          return logg('completed: pages cycle')
        if (!is_active())
          return logg('stopped: pages cycle')

        if (localStorage.exodus_comments_mode === 'true') {
          // we are on the post page, will download next comment page:
          const pagesLinks = getCommentsPaginationItems()
          if (pagesLinks) {
            const nextPageIndex = findNextCommentPage(pagesLinks)
            if (nextPageIndex === undefined || nextPageIndex >= pagesLinks.length) {
              logg('no more pages in comments')
            } else {
              let page = pagesLinks[nextPageIndex];
              logg(`downloading post: ${page.href}`)
              download_comment_page(page.href, page.innerHTML)
              await sleep(2000)
              page.click()
              await sleep(5000)
            }
          }

          logg('all comments pages were downloaded. going back...')
          localStorage.exodus_comments_mode = false
          await sleep(2000)
          window.location.assign(localStorage.exodus_page);
          await sleep(2000)
        } else {
          logg('not a comment mode')

          let all_posts = document.querySelectorAll('.postLinksBackg .urlLink a')
          if (!all_posts)
            throw "не нашёл ни одного поста на странице"

          // due to network error, sometimes after clicking <a> next link
          // you might stay on same page
          let first_post = all_posts[0].href
          let p = localStorage.exodus_prev_page_first_post
          if (p && p == first_post)
            throw "скрипт попытался обработать одну и ту же страницу дважды. Чаще всего причиной этого " +
            "является 'особенность поведения' пагинации на этом сайте. Попробуйте добавить несколько постов" +
            " в дневник, это может помочь исправить проблему пагинации. " +
            "Убедитесь, что нажимая на страницу 2 дневника, вы оказываетесь на ней, а не на предыдущей." +
            " Затем повторите импорт дневника."

          while (true) {
            if (is_completed())
              return logg('completed: posts cycle')
            if (!is_active())
              return logg('stopped: posts cycle')

            let next_post = find_next_post(all_posts)
            if (!next_post)
              break;

            logg(`next post found: ${next_post}`)
            download_post(next_post.href)
            await sleep(1000)
          }

          logg('no more posts')


          while (true) { //iterating over comments with multiple pages
            logg('checking comments pages...')
            const counts = document.querySelectorAll('.comments_count_link a:first-child')
            const next_comment = find_next_comment(counts)
            if (!next_comment) {
              logg(`all comments processed`)
              localStorage.exodus_last_processed_comment = '';
              break;
            } else {
              localStorage.exodus_last_processed_comment = next_comment.href;

              let commentsNumber = getCommentsNumber(next_comment);
              console.log(
                'commentsNumber', commentsNumber)
              if (commentsNumber > 30) {
                logg(`found multiple pages in comments: ${next_comment}`)
                localStorage.exodus_comments_mode = 'true'
                await sleep(2000)
                const comment_page = next_comment.href.replace('?all=true', '');
                logg(`opening comment page: ${comment_page}`)
                window.location.assign(comment_page);

                await sleep(2000)
              }
            }
          }

          logg('finished page parsing')
          await sleep(10000)
          let has_next_page = go_to_next_page()
          if (!has_next_page)
            break
          localStorage.exodus_prev_page_first_post = first_post
          await sleep(10000)
        }
      }
      localStorage.exodus_is_completed = true
      localStorage.exodus_is_active = false
      alert("Импорт закончен.")
    } catch (e) {
      alert(`Ошибка скрипта: ${e}.\nВ консоли (F12) могут быть полезные подробности.`)
      import_stop()
    }
  }

  function getCommentsNumber(next_comment) {
    try {
      const matches = next_comment.innerHTML.match(/^\d+/g)
      if (matches && matches.length > 0) {
        return Number(matches[0])
      }
      return 0;
    } catch (e) {
      logg(`commentCounter was not a number: ${next_comment.innerHTML}`)
    }
    return 0;
  }

  function find_next_post(all_posts) {
    const last_processed = last_processed_post()
    if (last_processed) {
      logg(`last post is ${last_processed}, finding next`)
      return find_after(all_posts, last_processed)
    }
    logg("no last post stored, assuming first time on this page")
    return all_posts[0]
  }

  function find_next_comment(all_comments) {
    const last_processed = localStorage.exodus_last_processed_comment
    if (last_processed) {
      logg(`last post is ${last_processed}, finding next`)
      return find_after(all_comments, last_processed)
    }
    logg("no last comment link stored, assuming first time on this page")
    return all_comments[0]
  }

  function find_after(queried, last) {
    for (let i = 0; i < queried.length; i++) {
      if (queried[i].href === last) {
        if (i + 1 < queried.length) {
          return queried[i + 1]
        }
        return null
      }
    }
    throw `не нашёл на странице предыдущий сохранённый пост ${last}`
  }

  function getPaginationItems() {
    try {
      return document.querySelectorAll('.pageBar>.pagination')[0].children
    } catch (e) {
      logg(`pagination not found: ${e}.`)
    }
  }

  function getCommentsPaginationItems() {
    try {
      return document.querySelectorAll('.pagination')[0].children
    } catch (e) {
      logg(`pagination not found: ${e}.`)
    }
  }

  const findNextCommentPage = (pages) => {
    for (let i = 0; i < pages.length; i++) {
      if (pages[i].className === "active") {
        return i + 1
      }
    }
    throw "не нашёл текущую страницу в разделе пагинации"
  }

  const findNextPage = (pages) => {
    for (let i = 0; i < pages.length; i++) {
      if (pages[i].className === "active") {
        if (i === 0 && pages.length > 2) {
          // active points at "last", so we want to switch to "2" instead of "1"
          // to partially avoid posts duplication
          return i + 2
        }
        return i + 1
      }
    }
    throw "не нашёл текущую страницу в разделе пагинации"
  }

  function go_to_next_page() {
    logg('going to next page')
    localStorage.removeItem('exodus_last_processed_post')
    const pages = getPaginationItems();
    if (!pages) {
      alert('Не найден раздел со страницами дневника.\n'
        + 'Либо в дневнике всего 1 страница, либо в скрипте возникла ошибка.')
      return false
    }

    const nextPageIndex = findNextPage(pages)
    if (nextPageIndex === undefined || nextPageIndex >= pages.length) {
      logg('no more pages')
      return false
    }
    localStorage.exodus_page = pages[nextPageIndex].href
    logg(`clicking next: ${localStorage.exodus_page}`)
    pages[nextPageIndex].click()
    return true
  }

  // start downloading post by HTML element
  function download_comment_page(url, page) {
    console.assert(url)
    const split = url.split('/')
    const filename = `${split[split.length - 1]}_page${page}`
    perform_url_download(url, filename)
  }

  // start downloading post by HTML element
  function download_post(url) {
    console.assert(url)
    const split = url.split('/')
    const filename = split[split.length - 1]
    // console.log('request all comments:', url_all_comments)
    perform_url_download(url, filename)
    localStorage.exodus_last_processed_post = url
  }

  // actually download data by js/browser magic
  function perform_url_download(url, filename) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4 && xhr.status == 200) {
        const u = window.URL.createObjectURL(new Blob([xhr.responseText], {type: "text/html"}))
        const link = document.createElement('a')
        link.href = u
        link.setAttribute('download', filename)
        link.click();
      }
    }
    xhr.open("GET", url, false)
    xhr.send();
  }

  async function sleep(x) {
    return new Promise(r => setTimeout(r, x))
  }

  function logg(x) {
    console.debug(`Exodus: ${x}`)
  }

  GM_registerMenuCommand("Проверить состояние", import_status, '1')
  GM_registerMenuCommand("Начать импорт", import_start, '2')
  GM_registerMenuCommand("Приостановить импорт", import_stop, '3')
  GM_registerMenuCommand("Прервать импорт", import_abort, '4')

  if (is_active()) {
    logg('going to parse current window')
    parse_current_window()
  }
})();
