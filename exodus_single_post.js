// ==UserScript==
// @name            Download a post with multiple-paged comments
// @namespace       http://tampermonkey.net/
// @description     Context menu to execute UserScript
// @version         0.0
// @author          Artalus & Denolia
// @include         /https?:\/\/(?!secure).*\.?diary.ru(\/.*)?/
// @grant           GM_registerMenuCommand
// @noframes
// ==/UserScript==]


(function() {
  'use strict';
  let is_active = () => (localStorage.exodus_is_active === 'true')
  let is_completed = () => (localStorage.exodus_is_completed === 'true')
  let was_in_process = () => (localStorage.exodus_diaryname !== undefined)
  let current_diary = () => (localStorage.exodus_diaryname)

  let page = () => (localStorage.exodus_page)
  let last_processed_post = () => (localStorage.exodus_last_processed_post)
  let working_post = () => (localStorage.exodus_working_post)

  logg(window.location.href)

  // UI: display alert with all possible info about import
  function import_status() {
    let msg = ''
    const URL = window.location.href
    msg += `Текущая веб-страница: ${URL}\n`
    const m = URL.match(/^https?:\/\/(.+).diary.ru/)
    if (!m) {
      msg += 'Вы находитесь не на diary.ru'
      alert(msg)
      return
    }
    msg += 'Процесс импорта дневника:\n'
    const d = current_diary()
    if (d)
      msg += (`- импортируемый дневник: ${d}.diary.ru\n`)
    else
      msg += '- дневник не выбран\n'
    if (is_active()) {
      msg += '- сейчас активен - продолжится АВТОМАТИЧЕСКИ при заходе на страницу дневника!\n'
    } else {
      msg += '- сейчас неактивен - можно безопасно заходить на страницу дневника\n'
      if (was_in_process())
        msg += '- ранее был запущен\n'
      else
        msg += '- ранее не запускался\n'
    }
    if (is_completed()) {
      msg += '- успешно завершён\n'
    } else {
      const p = page()
      if (p)
        msg += `- обрабатываемая страница: ${p}\n`
      const l = last_processed_post()
      if (l)
        msg += `- последний импортированный пост: ${l}\n`
      const w = working_post()
      if (w)
        msg += `- пост к обработке: ${w}\n`
    }
    alert(msg)
  }

  // UI: abort import and erase all data about process
  function import_abort() {
    const yes = confirm(`Это полностью прервёт импорт дневника ${current_diary()} и сотрёт данные о процессе.\nУверены?`)
    if (!yes)
      return
    remove_local_data()
    logg('import aborted')
    alert('Процесс импорта полностью остановлен')
  }

  function remove_local_data() {
    localStorage.removeItem('exodus_is_active')
    localStorage.removeItem('exodus_is_completed')
    localStorage.removeItem('exodus_diaryname')
    localStorage.removeItem('exodus_page')
    localStorage.removeItem('exodus_last_processed_post')
    localStorage.removeItem('exodus_working_post')
    localStorage.removeItem('exodus_prev_page_first_post')
    localStorage.removeItem('exodus_comments_mode')
    localStorage.removeItem('exodus_last_processed_comment')
  }

  // UI: begin/continue import
  function import_start() {
    logg('import_start()')

    const previous = current_diary()
    if (is_active()) {
      alert(`Уже идёт импорт дневника ${previous}.\n`
        +"Вначале прервите его или поставьте на паузу")
      return
    }

    const URL = window.location.href
    const m = URL.match(/^https?:\/\/(.+).diary.ru/)
    if (!m) {
      alert(`Вы находитесь не на diary.ru, а на ${URL}`
        +'\nВначале зайдите на свой дневник, например foobar.diary.ru')
      return
    }
    const selected = m[1]

    if (is_completed()) {
      const yes = confirm(`Дневник ${selected} уже был скачан.\nНачать сначала?`)
      if (!yes)
        return
      remove_local_data()
    } else {
      const yes = confirm(`Будет импортирован дневник ${selected}.\nУверены?`)
      if (!yes)
        return
    }
    localStorage.exodus_is_active = true
    localStorage.exodus_diaryname = selected
    logg('starting import')
    alert(`Начинаем импорт дневника ${selected}.`)
    if (!page()) {
      logg(`no page; starting a new import. setting current page:\n${URL}`)
      localStorage.exodus_page = URL
    }
    parse_current_window()
  }

  // UI: pause import until next "start" action
  function import_stop() {
    logg('stopping import')
    localStorage.exodus_is_active = false
    alert(`Импорт дневника ${current_diary()} остановлен`)
  }

  // main worker function
  // async to be able to sleep() inside and catch exceptions from inner functions
  async function parse_current_window() {
    try {

      logg(`page() ${page()}`)
      logg(`comment mode: ${localStorage.exodus_comments_mode}`)

      if (is_completed())
        return logg('completed: pages cycle')
      if (!is_active())
        return logg('stopped: pages cycle')

      // we are on the post page, will download next comment page:
      const pagesLinks = getCommentsPaginationItems()
      if (pagesLinks) {
        const currentPageIndex = findCurrentCommentPage(pagesLinks)
        if (currentPageIndex === undefined || currentPageIndex >= pagesLinks.length){
          logg('cannot find current page in pagination')
        } else {
          let page = pagesLinks[currentPageIndex];
          logg(`current comment page: ${page.href}`)
          download_comment_page(page.href, page.innerHTML)
        }
        const nextPageIndex = findNextCommentPage(pagesLinks)
        if (nextPageIndex === undefined || nextPageIndex >= pagesLinks.length){
          logg('no more pages in comments')
        } else {
          const nextPage = pagesLinks[nextPageIndex];
          logg('will go to next page', nextPage.href)
          nextPage.click()
          await sleep(5000)
        }

        logg('all comments pages were downloaded. going back...')
        await sleep(2000)
      }
      localStorage.exodus_is_completed = true
      localStorage.exodus_is_active = false
      alert("Импорт закончен.")
    } catch(e) {
      alert(`Ошибка скрипта: ${e}.\nВ консоли (F12) могут быть полезные подробности.`)
      import_stop()
    }
  }

  function getCommentsPaginationItems() {
    try {
      return document.querySelectorAll('.pagination')[0].children
    } catch (e) {
      logg(`pagination not found: ${e}.`)
    }
  }
  const findCurrentCommentPage = (pages) => {
    for (let i = 0; i < pages.length; i++) {
      if (pages[i].className === "active") {
        return i
      }
    }
    throw "не нашёл текущую страницу в разделе пагинации"
  }

  const findNextCommentPage = (pages) => {
    for (let i = 0; i < pages.length; i++) {
      if (pages[i].className === "active") {
        return i + 1
      }
    }
    throw "не нашёл текущую страницу в разделе пагинации"
  }

  // start downloading post by HTML element
  function download_comment_page(url, page) {
    console.assert(url)
    logg(`download_comment_page: ${url}`)
    const split = url.split('/')
    const filename = `${split[split.length - 1]}_page${page}`
    perform_url_download(url, filename)
  }

  // start downloading post by HTML element
  function download_post(url) {
    console.assert(url)
    const split = url.split('/')
    const filename = split[split.length-1]
    perform_url_download(url, filename)
    localStorage.exodus_last_processed_post = url
  }

  // actually download data by js/browser magic
  function perform_url_download(url, filename) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      if (xhr.readyState == 4 && xhr.status == 200) {
        const u = window.URL.createObjectURL(new Blob([xhr.responseText], {type:"text/html"}))
        const link = document.createElement('a')
        link.href = u
        link.setAttribute('download', filename)
        link.click();
      }
    }
    xhr.open("GET", url, false)
    xhr.send();
  }

  async function sleep(x) {
    return new Promise(r => setTimeout(r, x))
  }

  function logg(x) {
    console.debug(`Exodus: ${x}`)
  }

  GM_registerMenuCommand("Проверить состояние", import_status, '1')
  GM_registerMenuCommand("Начать импорт", import_start, '2')
  GM_registerMenuCommand("Приостановить импорт", import_stop, '3')
  GM_registerMenuCommand("Прервать импорт", import_abort, '4')

  if (is_active()) {
    logg('going to parse current window')
    parse_current_window()
  }
})();
